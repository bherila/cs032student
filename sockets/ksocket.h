#ifndef KSOCKET_H
#define KSOCKET_H

#include "jsocket.h"

class kSocket : public jSocket {

    kSocket* clone(int fd);

protected:
    kSocket(bool b, int fd);

public:
    kSocket(uint16_t port) : jSocket(port) {}

    /* TODO: Implement your extensions to jsocket here, e.g. readLine, << or >> */

};

#endif
