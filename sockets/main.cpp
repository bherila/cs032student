#include "jsocket.h"
#include "ksocket.h"

#include <iostream>
#include <string>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

char welcomeMessage[] = "Welcome! You've got mail!\n";

void quitHandler(int sig){
    (void) sig; /* avoid warning about unused parameter */

    printf("Quitting because of CTRL+C...\n");

    /* TODO: Perform cleanup here */

    exit(0);
}

int main(int argc, char **argv) {
    signal(SIGINT, quitHandler);

    uint16_t port = 9999;

    /* Read port number from command line, if provided */
    if (argc > 1) {
        uint16_t t = static_cast<uint16_t>(atoi(argv[1]));
        if (t > 1000) {
            port = t;
        }
        else {
            fprintf(stderr, "Invalid port: %s \n", argv[1]);
            exit(-2);
        }
    }

    kSocket *server = new kSocket(port);
    fprintf(stderr, "Now listening on port %d \n", (int)port);

    while(true) {

        kSocket *client = reinterpret_cast<kSocket *>(server->accept());

        /* Print a message proving that we connected */
        fprintf(stderr, "connected\n");
        fflush(stderr);

        /* TODO: (Optional) Send client a welcome message */

        /* TODO: Spawn a new thread (use QThread) for this client */

        /* TODO: Close and delete client when finished */

    }

    server->close();
    delete server;

    return 0;
}
