#include "jsocket.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

#include <iostream>
using namespace std;

jSocket::jSocket(uint16_t port)
  : fd(-1), eof(false), waiting("") {

  fd = tcpsocket();

  /* The following allows you to rebind a TCP port when old
   * connections are still in TIME_WAIT.  */
  int n = 1;
  setsockopt (fd, SOL_SOCKET, SO_REUSEADDR, (char *) &n, sizeof (n));

  struct sockaddr_in sin;
  memset(&sin, 0, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_port = htons (port);
  sin.sin_addr.s_addr = htonl (INADDR_ANY);
  if (bind (fd, (struct sockaddr *) &sin, sizeof (sin)) < 0) {
    perror ("bind");
    exit (1);
  }

  if (listen (fd, 5) < 0) {
    perror ("listen");
    exit (1);
  }
}

jSocket::jSocket(const std::string& host, const std::string& port)
  : fd(-1), eof(false), waiting("") {

  struct addrinfo hints;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;     /* Allow IPv4 or IPv6 */
  hints.ai_socktype = SOCK_STREAM; /* Stream (TCP) socket */

  struct addrinfo* result;
  if (int ret = getaddrinfo(host.c_str(), port.c_str(), &hints, &result)) {
    cerr << "getaddrinfo: " << gai_strerror(ret) << endl;
    exit(1);
  }

  for (struct addrinfo* ap = result; ap != NULL; ap = ap->ai_next) {
    int sfd = socket(ap->ai_family, ap->ai_socktype, ap->ai_protocol);
    if (sfd == -1)
      continue;

    if (connect(sfd, ap->ai_addr, ap->ai_addrlen) != -1) {
      fd = sfd;
      break;                  /* Success */
    }

    ::close(sfd);
  }
  freeaddrinfo(result);           /* No longer needed */


  if (fd == -1) {
    cerr << "Unable to connect to " << host << " " << port << endl;
    exit(1);
  }
}

int jSocket::tcpsocket() {
  int s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    perror ("socket");
    exit (1);
  }
  return s;
}

jSocket::~jSocket() {
  close();
}

jSocket* jSocket::accept() {
  struct sockaddr_in sin;
  memset(&sin, 0, sizeof(sin));
  socklen_t sinlen = sizeof (sin);
  int csock = ::accept (fd, (struct sockaddr *) &sin, &sinlen);
  if (csock < 0) {
    perror ("accept");
    exit (1);
  }

  return clone(csock);
}

// This pair is overridden in subclasses so accept() can return the subclass
jSocket* jSocket::clone(int fd) {
  return new jSocket(true, fd);
}
jSocket::jSocket(bool, int fd)
  : fd(fd), eof(false), waiting("") {
}



std::string jSocket::read() {
  if (waiting.length() > 0) {
    string ret = waiting;
    waiting = "";
    return ret;
  }

  if (eof)
    throw "EOF!";
  char buffer[4096];
  ssize_t ret = ::read(fd, buffer, sizeof(buffer));
  if (ret < 0) {
    perror("read");
    exit(1);
  }
  if (ret == 0) {
    eof = true;
    return "";
  }
  waiting = "";
  return string(buffer, ret);
}


void jSocket::write(const char* buf, int len) {
  const char* pos = buf;
  const char* end = pos + len;
  while (pos < end) {
    int written = ::write(fd, pos, end - pos);
    if (written < 0) {
      perror("write");
      exit(1);
    }
    pos += written;
  }
}


void jSocket::close() {
  if (fd == -1)
    return;
  ::close(fd);
  fd = -1;
}
