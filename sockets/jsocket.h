#ifndef JSOCKET_H
#define JSOCKET_H

#include <string>

class jSocket {
    int fd;
    bool eof;
    std::string waiting;

public:
    jSocket(uint16_t port);       // Java's "ServerSocket"
    jSocket(const std::string& host, const std::string& port); // Java's "Socket"
    ~jSocket();                   // Will close the fd. (RAII)

    jSocket* accept(); // Accepts and returns a new jSocket. See clone().

    /*! @brief Writes the entire given buffer of bytes */
    void write(const char* buf, int len);

    /*! @brief Reads waiting bytes from the socket, unless EOF */
    std::string read();

    /*! @brief Returns true if there is no more data to read */
    bool done() { return eof; }

    /*! @brief Closes the connection */
    virtual void close();

protected:
    /*! @brief Make a socket of the same type, new fd. */
    virtual jSocket* clone(int fd);

    jSocket(bool, int fd);

private:
    static int tcpsocket();

};

#endif
