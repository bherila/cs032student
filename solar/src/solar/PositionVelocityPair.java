/********************************************************************************
 * @author bherila
 * File:   Vector.java
 * @date   7 Feb 2011
 * Asgn:   Solar
 ********************************************************************************/

package solar;

/**
 * Pair which holds a position and a velocity.
 * @author bherila
 */
public class PositionVelocityPair {

    public Vector3 pos;
    public Vector3 vel;
}