/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package solar;

/**
 * Represents anything with an X, Y, and Z component.
 * @author bherila
 */
public interface IPositionable {

    double getX();
    double getY();
    double getZ();

}
