/********************************************************************************
 * @author bherila
 * File:   SolarPhysics.java
 * @date   23 January 2011
 * Asgn:   Solar
 ********************************************************************************/
package solar;

import solardraw.*;

public class SolarPhysics {

    /**
     * Sphere-sphere collision constant (k in relation d < k (r1 * r2))
     */
    public static final double K = 10;
    /**
     * The number of seconds that pass for each frame of the simulation
     */
    public static double timestep = 1200;

    /**
     * Calculates the magnitude and direction of the force between two objects.
     * @param x1 The position of the first object
     * @param m1 The mass of the first object
     * @param x2 The position of the second object
     * @param m2 The mass of the second object
     */
    public static Vector3 calculateForce(Vector3 x1, double m1, Vector3 x2, double m2) {

        Vector3 d = x1.subtract(x1);                /* displacement is x2 - x1 */
        double r2 = d.magnitude2();                 /* distance ^2 */
        double G = SolarConstants.GRAVITY_CONSTANT; /* G from formula */
        double F = G * m1 * m2 / r2;                /* G m1 m2 / r^2 */

        return d.getNormalized().scalarMultiply(F);
    }

    /**
     * Calculates the new position and velocity of the given object based on the sum of the forces
     * exerted on it.
     * @param position The current position of the object.
     * @param velocity The current velocity of the object.
     * @param force The sum of the forces being exerted on the object
     * @param mass The mass of the object
     */
    public static PositionVelocityPair updatePosition(Vector3 position, Vector3 velocity, Vector3 force, double mass) {
        Vector3 a = force.scalarMultiply(1.0 / mass); /* A = F/M */

        PositionVelocityPair result = new PositionVelocityPair();
        result.vel = velocity
                .add(a.scalarMultiply(timestep)); /* v = v + at */

        /*  x = x + vt + (1/2)at^2  */
        result.pos = position
                .add(result.vel.scalarMultiply(timestep))
                .add(a.scalarMultiply(0.5).scalarMultiply(timestep));

        return result;
    }

    /**
     * Calculates an initial velocity that will put an object into stable orbit
     * @param position The position of the object
     * @param mass The mass of the object
     * @param sunPosition The position of the sun
     * @param sunMass
     */
    public static Vector3 calculateOrbitVelocity(
            Vector3 position, double mass,
            Vector3 sunPosition, double sunMass) {

        /* From the TAs: Fear not, for there are no bugs in this method */
        final double G = SolarConstants.GRAVITY_CONSTANT;
        Vector3 dist = sunPosition.subtract(position);
        double sForce = (G * mass * sunMass) / dist.magnitude2();
        Vector3 direction = dist.cross(Vector3.getRandom()).getNormalized();
        return direction.scalarMultiply(Math.sqrt(sForce * dist.magnitude() / mass));
    }
}
