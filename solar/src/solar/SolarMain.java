/********************************************************************************
 * @author kdoo, dkilian, bherila
 * File:   SolarMain.java
 * @date   4 February 2009, 23 January 2011, 7 February 2011
 * Asgn:   Solar
 ********************************************************************************/
package solar;

import solardraw.*;

import jargs.*;

/**
 * SolarMain is a class that simulates a Solar System.
 * There are 4 other classes that are used in addition to this one that 
 * provide functionalities.
 */
public class SolarMain implements SolarDraw.Control {

    /**
     * Creates a SolarMain.  You will want to pass in arguments for
     * the initial state of the simulation and it may be helpful to
     * have more than one constructor.
     */
    public SolarMain() {
    }

    /**
     * Start and initialize SolarMain
     *
     * @param args - input and/or ouput file -- see below for details.
     */
    public static void main(String[] args) {

        CmdLineParser parser = new CmdLineParser();
        CmdLineParser.Option inputFlag = parser.addStringOption('i', "input");
        CmdLineParser.Option outputFlag = parser.addStringOption('o', "output");
        CmdLineParser.Option numPlanetsFlag = parser.addIntegerOption('p', "planets");
        CmdLineParser.Option numRocketsFlag = parser.addIntegerOption('r', "rockets");
        CmdLineParser.Option numTimestepsFlag = parser.addIntegerOption('t', "timesteps");
        CmdLineParser.Option closeWhenDoneFlag = parser.addBooleanOption('c', "close");

        try {
            parser.parse(args);
        } catch (CmdLineParser.OptionException e) {
            System.err.println(e.getMessage());
        }

        String inputFile = (String) parser.getOptionValue(inputFlag);
        String outputFile = (String) parser.getOptionValue(outputFlag);
        int numPlanets = (Integer) parser.getOptionValue(numPlanetsFlag, 200);
        int numRockets = (Integer) parser.getOptionValue(numRocketsFlag, 100);

        if (outputFile == null) {
            outputFile = "default.xml";
        }
        int numTimesteps = (Integer) parser.getOptionValue(numTimestepsFlag, 120000);

        boolean closeWhenDone = (Boolean) parser.getOptionValue(closeWhenDoneFlag, false);


        //Create your SolarMain instance here. You will want to modify the constructor
        //to take in parameters.
        new SolarMain();
    }

    @Override
    public double getTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void saveCalled() {
        // TODO Auto-generated method stub
    }
}
