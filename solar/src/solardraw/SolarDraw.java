/*********************************************************************************
 *
 *        SolarDraw.java
 *
 *    Drawing module for the solar system
 *
 *********************************************************************************
 *    Copyright 2003 Brown University -- Steven P. Reiss
 *********************************************************************************
 *  Copyright 2003, Brown University, Providence, RI.
 *
 *              All Rights Reserved
 *
 *  Permission to use, copy, modify, and distribute this software and its
 *  documentation for any purpose other than its incorporation into a
 *  commercial product is hereby granted without fee, provided that the
 *  above copyright notice appear in all copies and that both that
 *  copyright notice and this permission notice appear in supporting
 *  documentation, and that the name of Brown University not be used in
 *  advertising or publicity pertaining to distribution of the software
 *  without specific, written prior permission.
 *
 *  BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 *  SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR ANY PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY
 *  BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
 *  DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 *  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 *  ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 *  OF THIS SOFTWARE.
 *
 ********************************************************************************/
/* RCS: $Header$ */
/*********************************************************************************
 *
 * $Log$
 *
 ********************************************************************************/
package solardraw;
/**
 * This interface is designed to be used by a program that is computing the
 * position of a number of objects and wants to have a 3D display of the result.
 *
 * Author: Steven P. Reiss
 *
 **/
public interface SolarDraw {
    /********************************************************************************
    /*
    /* Abstract methods
    /*
    /********************************************************************************/
    /**
     * Register an object to be drawn.
     **/
    void registerObject(SolarDraw.Body sdo);
    void unregisterObject(SolarDraw.Body sdo);
    /**
     * Call this every iteration of your main loop to calculate UPS.
     */
    void tick();
    /**
     * Call this to see how many ticks have elapsed.
     *
     * @return - the number of ticks since start of simulation.
     */
    long getTicks();
    /**
     * Start up the drawing. The user should first register all the objects
     * before calling this routine
     **/
    void begin();
    /**
     * Determine the correct name to assign to the new combined object when two objects
     * collide. PLEASE USE THIS or it will make it difficult for us to grade your
     * simulation.
     *
     * @param collider_name_1 name of first colliding object
     * @param collider_name_2 name of second colliding object
     * @return name to give two resultant combined object
     */
    String getCombinedBodyName(String collider_name_1, String collider_name_2);
    /********************************************************************************
    /*                                                                              *
    /* Object interface: user object representation                                 *
    /*                                                                              *
    /********************************************************************************/
    /**
     * This interface is designed to be implemented by the client. The client
     * has some object representation (presumably). This object should implement
     * SolarDraw.Object so it can provide the information needed by the display
     * package. Note that setting the mass to zero causes the object to
     * disappear from the display.
     **/
    public enum Shape {
        Cone, Sphere
    }
    public enum CollisionBehavior {
        Explode, Combine, Shatter
    }
    public interface Body {
        public CollisionBehavior getCollisionBehavior();
        public String getName();
        public double getX();
        public double getY();
        public double getZ();
        public double getRadius();
        public double getMass();
        public Shape getShape();
    }
    /**
     * This interface should be implemented by the client. It provides the
     * information used by the drawing package to display system information.
     **/
    public interface Control {
        public double getTime();
        public void saveCalled();
    }
    /********************************************************************************
    /*
    /* Factory methods
    /*
    /********************************************************************************/
    /**
     * This class provides the means for creating the drawing package.
     **/
    public class Factory {
        public static SolarDraw createSolarDraw(SolarDraw.Control ctl) {
            return new SolarDrawImpl(ctl);
        }
    } // end of inner class Factory
} // end of interface SolarDraw
/* end of SolarDraw.java */
