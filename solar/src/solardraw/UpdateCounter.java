package solardraw;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Update Counter keeps track of UPS and the total number of updates
 * Note: You must Update after every iteration of your while loop to get accurate values from the Update Counter
 */
public class UpdateCounter {

    private long _total_updates;
    private Queue<Long> _times;
    private static int FRAMESIZE = 50;

    /**
     * Update Counter keeps track of UPS and the total number of updates
     * Note: You must Update the counter after every iteration of your while loop to get accurate values from the Update Counter
     */
    public UpdateCounter() {
        _total_updates = 0;
        _times = new LinkedList<Long>();

        for (int i = 0; i < FRAMESIZE; i++) {
            _times.add(1L);
        }
    }

    /**
     * Call this to update the counter
     */
    public void update() {
        synchronized (_times) {
            _total_updates++;
            _times.add(System.nanoTime());
            _times.remove();
        }

    }

    /**
     * Get the Updates-per-second from the Update Counter
     * @return the UPS
     */
    public double getUPS() {
        synchronized (_times) {
            return 1000000000d * (FRAMESIZE / (double) (System.nanoTime() - _times.peek()));
        }
    }

    /**
     * Get the number of total updates since the start of the simulation
     * @return the total number of updates
     */
    public long getTicks() {
        return _total_updates;
    }
}
