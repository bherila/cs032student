/********************************************************************************
/*
/*        SolarDrawVector.java
/*
/*    3D Vector for use in solar system drawer
/*
/********************************************************************************
/*    Copyright 2003 Brown University -- Steven P. Reiss
/*********************************************************************************
 *  Copyright 2003, Brown University, Providence, RI.
 *
 *              All Rights Reserved
 *
 *  Permission to use, copy, modify, and distribute this software and its
 *  documentation for any purpose other than its incorporation into a
 *  commercial product is hereby granted without fee, provided that the
 *  above copyright notice appear in all copies and that both that
 *  copyright notice and this permission notice appear in supporting
 *  documentation, and that the name of Brown University not be used in
 *  advertising or publicity pertaining to distribution of the software
 *  without specific, written prior permission.
 *
 *  BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 *  SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR ANY PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY
 *  BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
 *  DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 *  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 *  ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 *  OF THIS SOFTWARE.
 *
 ********************************************************************************/
/* RCS: $Header$ */
/*********************************************************************************
 *
 * $Log$
 *
 ********************************************************************************/
package solardraw;
class SolarDrawVector {
    /********************************************************************************/
    /*                                        */
    /*    Private Storage                             */
    /*                                        */
    /********************************************************************************/
    private double x_v;
    private double y_v;
    private double z_v;
    /********************************************************************************/
    /*                                        */
    /*    Constructors                                */
    /*                                        */
    /********************************************************************************/
    SolarDrawVector() {
        x_v = y_v = z_v = 0;
    }
    SolarDrawVector(double x, double y, double z) {
        x_v = x;
        y_v = y;
        z_v = z;
    }
    SolarDrawVector(SolarDrawVector v) {
        x_v = v.x_v;
        y_v = v.y_v;
        z_v = v.z_v;
    }
    /********************************************************************************/
    /*                                        */
    /*    Access methods                                */
    /*                                        */
    /********************************************************************************/
    public double getX() {
        return x_v;
    }
    public double getY() {
        return y_v;
    }
    public double getZ() {
        return z_v;
    }
    public double dsq() {
        return x_v * x_v + y_v * y_v + z_v * z_v;
    }
    public void set(double x, double y, double z) {
        x_v = x;
        y_v = y;
        z_v = z;
    }
    /********************************************************************************/
    /*                                        */
    /*    Operations                                */
    /*                                        */
    /********************************************************************************/
    public void addTo(SolarDrawVector v) {
        x_v += v.x_v;
        y_v += v.y_v;
        z_v += v.z_v;
    }
    public void subFrom(SolarDrawVector v) {
        x_v -= v.x_v;
        y_v -= v.y_v;
        z_v -= v.z_v;
    }
    public void divideBy(double c) {
        x_v /= c;
        y_v /= c;
        z_v /= c;
    }
    public void scaleBy(double c) {
        x_v *= c;
        y_v *= c;
        z_v *= c;
    }
    public void negate() {
        x_v = -x_v;
        y_v = -y_v;
        z_v = -z_v;
    }
    public void clear() {
        x_v = y_v = z_v = 0;
    }
    public void copyFrom(SolarDrawVector v) {
        x_v = v.x_v;
        y_v = v.y_v;
        z_v = v.z_v;
    }
    public void add(double x, double y, double z) {
        x_v += x;
        y_v += y;
        z_v += z;
    }
    /********************************************************************************/
    /*                                        */
    /*    Distance methods                            */
    /*                                        */
    /********************************************************************************/
    public double distance2(SolarDrawVector v) {
        double dx = x_v - v.x_v;
        double dy = y_v - v.y_v;
        double dz = z_v - v.z_v;
        return dx * dx + dy * dy + dz * dz;
    }
    public double distance2(double x, double y, double z) {
        double dx = x_v - x;
        double dy = y_v - y;
        double dz = z_v - z;
        return dx * dx + dy * dy + dz * dz;
    }
    void normalize() {
        double d = Math.sqrt(dsq());
        if (d != 0) {
            scaleBy(1 / d);
        }
    }
    void leftMult(SolarDrawMatrix m) {
        double rx = x_v * m.get(0, 0) + y_v * m.get(1, 0) + z_v * m.get(2, 0) + m.get(3, 0);
        double ry = x_v * m.get(0, 1) + y_v * m.get(1, 1) + z_v * m.get(2, 1) + m.get(3, 1);
        double rz = x_v * m.get(0, 2) + y_v * m.get(1, 2) + z_v * m.get(2, 2) + m.get(3, 2);
        x_v = rx;
        y_v = ry;
        z_v = rz;
    }
    /********************************************************************************/
    /*                                        */
    /*    Transformation methods                            */
    /*                                        */
    /********************************************************************************/
    public void linearOp(SolarDrawVector a, double m) {
        x_v += a.x_v * m;
        y_v += a.y_v * m;
        z_v += a.z_v * m;
    }
}    // end of class SolarDrawVector
/* end of SolarDrawVector.java */

