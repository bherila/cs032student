package solar;

import org.junit.*;
import static org.junit.Assert.*;

public class Vector3Tests {

    private Vector3 _v1, _vOrig;

    /**
     * Sets up the test fixture. 
     * (Called before every test case method.)
     */
    @Before
    public void setUp() {
        _v1 = Vector3.create(2, 7, -3);
        _vOrig = Vector3.create(2, 7, -3);
    }

    /**
     * Tears down the test fixture. 
     * (Called after every test case method.)
     */
    @After
    public void tearDown() {
        _v1 = null;
	    _vOrig = null;
    }

    /**
     * Tests Vector3.scalarMultiply(double scaleFactor)
     */
    @Test
    public void test_scalarMultiply() {

	    _v1 = _v1.scalarMultiply(1);
	    assertEquals(2, _v1.getX(), 1e-8);
	    assertEquals(7, _v1.getY(), 1e-8);
	    assertEquals(-3, _v1.getZ(), 1e-8);
    
	    _v1 = _v1.scalarMultiply(3);
	    assertEquals(6, _v1.getX(), 1e-8);
	    assertEquals(21, _v1.getY(), 1e-8);
	    assertEquals(-9, _v1.getZ(), 1e-8);
    
	    _v1 = _v1.scalarMultiply(0);
	    assertEquals(0, _v1.getX(), 1e-8);
	    assertEquals(0, _v1.getY(), 1e-8);
	    assertEquals(0, _v1.getZ(), 1e-8);
    
	    _v1 = _vOrig;
	    _v1 = _v1.scalarMultiply(-2);
	    assertEquals(-4, _v1.getX(), 1e-8);
	    assertEquals(-14, _v1.getY(), 1e-8);
	    assertEquals(6, _v1.getZ(), 1e-8);
    
	    _v1 = _vOrig;
	    _v1 = _v1.scalarMultiply(.5);
	    assertEquals(1, _v1.getX(), 1e-8);
	    assertEquals(3.5, _v1.getY(), 1e-8);
	    assertEquals(-1.5, _v1.getZ(), 1e-8);
    }

    /**
     * Tests Vector3.addVector(Vector3 toAdd)
     */   
    @Test
    public void test_addVector() {
        //TODO for youhoo
    }

}
