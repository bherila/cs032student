#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QRunnable>

class Counter;

class MyThread : public QRunnable
{
public:
    MyThread(Counter *counter);
    virtual ~MyThread();
    //TODO: Add methods here if needed


private:
    //TODO: Add instance variables here if needed

    Counter *m_counter;
};

#endif // MYTHREAD_H
