#ifndef COUNTER_H
#define COUNTER_H

class Counter
{
public:
    Counter();
    virtual ~Counter();
    void increment();
    int getCount();


private:
    //TODO: Add instance variables here


};

#endif // COUNTER_H
