#-------------------------------------------------
#
# Project created by QtCreator 2011-01-23T12:20:57
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Threads
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Counter.cpp \
    MyThread.cpp

HEADERS += \
    Counter.h \
    MyThread.h
