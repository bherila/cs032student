#include <QtCore/QCoreApplication>
#include <QThreadPool>
#include "Producer.h"
#include "Consumer.h"
#include "CubbyHole.h"

int main(int argc, char *argv[])
{
    CubbyHole ch;
    Producer *p = new Producer(&ch); /* QRunnables are auto-deleted, do NOT call delete p! */
    Consumer *c = new Consumer(&ch);

    QThreadPool::globalInstance()->start(p);
    QThreadPool::globalInstance()->start(c);

    QThreadPool::globalInstance()->waitForDone();

    return 0;
}
