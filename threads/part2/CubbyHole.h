#ifndef CUBBYHOLE_H
#define CUBBYHOLE_H

#include <QMutex>
#include <QWaitCondition>

class CubbyHole
{
public:
    CubbyHole();
    int get();
    void put(int value);

private:
    int m_contents;

    /* TODO: Put any new members in here!*/

    // begin ta code
    QMutex m_mutex;
    QWaitCondition m_cond;
    bool is_available;
    // end ta code
};

#endif // CUBBYHOLE_H
