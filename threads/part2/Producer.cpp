#include "Producer.h"
#include "CubbyHole.h"

Producer::Producer(CubbyHole* c) : m_cubby(c)
{
}

Producer::~Producer(){}

void Producer::run ()
{
    for (int i = 0; i < 10; ++i)
    {
        m_cubby->put(i);
    }
}
