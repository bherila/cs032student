#ifndef PRODUCER_H
#define PRODUCER_H

#include <QRunnable>

class CubbyHole;

class Producer : public QRunnable
{
public:
    Producer(CubbyHole* c);
    virtual ~Producer();
    void run();
private:
    CubbyHole* m_cubby;
};

#endif // PRODUCER_H
