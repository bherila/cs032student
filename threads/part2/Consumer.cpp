#include "Consumer.h"
#include "CubbyHole.h"

Consumer::Consumer(CubbyHole* c) : m_cubby(c)
{
}

Consumer::~Consumer() {}

void Consumer::run ()
{
    for (int i = 0; i < 10; ++i)
    {
        m_cubby->get();
    }
}
