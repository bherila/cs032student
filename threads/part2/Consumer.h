#ifndef CONSUMER_H
#define CONSUMER_H

#include <QRunnable>

class CubbyHole;

class Consumer : public QRunnable
{
public:
    Consumer(CubbyHole* c);
    virtual ~Consumer();
    void run();
private:
    CubbyHole* m_cubby;
};

#endif // CONSUMER_H
