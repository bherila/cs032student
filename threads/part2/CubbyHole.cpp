#include "CubbyHole.h"
#include <stdio.h>

CubbyHole::CubbyHole()
{

}

int CubbyHole::get()
{
    // TODO: Add code to this method.

    fprintf(stdout, "Consumer got %d \n", m_contents);
    fflush(stdout); // Flush the output stream just in case

    return m_contents;
}

void CubbyHole::put(int value)
{
    // TODO: Add code to this method. 

    fprintf(stdout, "Producer put %d\n", value);
    fflush(stdout); // Flush the output stream just in case

    m_contents = value;
}
