#-------------------------------------------------
#
# Project created by QtCreator 2011-03-13T12:17:00
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = pc
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Producer.cpp \
    CubbyHole.cpp \
    Consumer.cpp

HEADERS += \
    Producer.h \
    CubbyHole.h \
    Consumer.h
