package cs032.support;

import java.awt.Point;

public interface BattleshipPlayer{

	/**
	 * Initializes your BattleshipPlayer, and populates the Board with 
	 * your ships. Be sure to place your ships on the board here!
	 * 
	 * @see Board.placeShip(Point[] c);
	 * @see Board.placeShip(Collection<Point> c);
	 * 
	 * @param b: your Board.  Populate it.
	 */
	public void init(Board b);
	/**
	 * Returns a Point representing your next move: the space on your opponent's board
	 * which you would like to hit.  The game engine will call this method.
	 * 
	 * @return your next target
	 */
	public Point getNextMove();
	/**
	 * This is a callback; the game engine will call this method after calling getNextMove().
	 * 
	 * @param b: true if the last move was a hit, false if the last move was a miss.
	 */
	public void lastMoveResult(boolean b);
    
}