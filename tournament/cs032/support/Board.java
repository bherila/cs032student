package cs032.support;

import java.awt.Point;
import java.util.Collection;

public interface Board {

	/**
	 * Takes an Array of points representing a ship, 
	 * and attempts to place it on the board.
	 * Returns true when the desired placement is 
	 * valid, and returns false when it is invalid. 
	 * 
	 * If placeShip returns false, your ship was
	 * not placed.
	 * 
	 * @param c: the ship
	 * @return whether attempt was valid
	 */
	public boolean placeShip(Point[] c);
	
	/**
	 * Takes a Collection of points representing a ship, 
	 * and attempts to place it on the board.
	 * Returns true when the desired placement is 
	 * valid, and returns false when it is invalid. 
	 * 
	 * If placeShip returns false, your ship was
	 * not placed.
	 * 
	 * @param c: the ship
	 * @return whether attempt was valid
	 */
	public boolean placeShip(Collection<Point> c);
	
	/**
	 * Takes a Point representing a location on your 
	 * opponent's grid and fires at it.  Returns
	 * true if your shot hit, returns false 
	 * if your shot missed or was invalid.
	 * 
	 * @param p Point you'd like to hit
	 * @return whether you hit a ship
	 */
	public boolean fire(Point p);

}
